@echo off
if "%2" EQU "" (
    set PARAM1=HEAD
    set PARAM2=%1
) else (
    set PARAM1=%1
    set PARAM2=%2
)

echo %3 | find /N "wsl" >NUL
if not ERRORLEVEL 1 (
    pushd %3
)

"C:\Users\ss764\AppData\Local\Atlassian\SourceTree\git_local\bin\git.exe" config --global core.quotepath false
chcp 65001
setlocal enabledelayedexpansion
set YYYYMMDD=%DATE:/=%
set RET_DIR=
for /F "usebackq" %%i in (`"C:\Users\ss764\AppData\Local\Atlassian\SourceTree\git_local\bin\git.exe" diff --name-only %PARAM2% %PARAM1% --diff-filter=ACMR`) do (
    set RET_DIR=!RET_DIR! "%%i"
)

"C:\Users\ss764\AppData\Local\Atlassian\SourceTree\git_local\bin\git.exe" archive --format=zip %PARAM1% %RET_DIR% -o exported_diff_%YYYYMMDD%.zip